const express = require("express");
const utils = require("./utils");

const app = express();
app.use(express.json());

const port = 1234;

app.listen(port, () => console.log(`Serverul merge pe portul ${port}`));

app.get("/tema", async (req, res) => {
  //functiile la care aveți acces din fișierul utils sunt:
  //resetDatabase(),
  //getAllActors(),
  //getActorById(id),
  //insertActor(actor),
  //deleteActorById(id)
  //getAllMovies(),
  //getMovieById(id),
  //insertMovie(movie)

  //Multă baftă!

  res.status(200).send("Chestia asta va apărea în browser 🤠");
});
