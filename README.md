# Temă individuală IT Back-End
## _Bine ați venIT, rău ați nimerIT 🐊_
![](https://media.giphy.com/media/SXJ5PXwRbRvQAJt2N7/giphy.gif)

Salutare dragi aspiranți la mai mult. Tematica de astăzi? filme 🎥 și oameni care joacă în filme 🎭:
--a venit momentul să aplicați tot ce ați învățat la AC 📚. Drept delegat al Departamentului IT, divizia Back-End, va trebui să te ocupi să faci niște rute, o bază de date ~ tot alaiul 🎉.

![](https://media.giphy.com/media/kQ3FSVoJrkYWk/giphy.gif)
### Definirea termenilor

Ce este un **actor** 🤔? Ei bine, conform [Dexonline](dexonline.ro) un actor este:
> Artist(ă) care interpretează roluri 
> în piese de teatru, în filme etc.

Noi vom folosi astăzi o altă definiție, pentru contextul nostru un `Actor` este un obiect care conține următoare câmpuri:
```
{
    firstName: STRING,
    lastName: STRING,
    email: STRING,
    phone: STRING,
    sex: STRING,
    role: STRING,
    salary: INT,
    dateOfBirth: DATE,
    isDebutante: BOOL,
    movieId: INT
}
```




Acum haideți să vedem ce este un **Film** 🎬. Poate puțin predictibil, `Film` este tot un obiect ce conține *cel puțin* următoarele câmpuri

```
{
    title: STRING,
    description: STRING,
    duration: INT,
    dateOfRelease: DATE,
    genre: STRING,
    budget: INT,
    rating: INT,
    hasAward: BOOL,
    director: STRING
}
```


#### **Observații**: 
1. în baza de date aceste obiecte vor avea și un Id 🆔, care va fi incrementat++ automat atunci când le introduceți.
2. între tabela `Movies` și `Actors` există o relație de tipul `One-To-Many`, mai multe detalii [aici](https://en.wikipedia.org/wiki/One-to-many_(data_model)).

# Set-up
![](https://media.giphy.com/media/l0MYyztZ0Hg0CD6Ks/giphy.gif)
1. În primul rând, pentru a putea descărca repository-ul trebuie să deschidem Git Bash la locația pe care ne-o dorim
2. Folosiți comanda `git clone https://gitlab.com/MrMorningstarL/tema-individuala_back_2023_cod.git`
3. După ce ni s-a descărcat proiectul trebuie să intrăm în folder-ul nou creat, cu `cd tema-individuala_back_2023_cod`
4. Acum, ca să începem să putem scrie cod, deschidem Visual Studio Code folosind `code .`
5. Îm VS Code vom porni un terminal, cu ajutorul căruia ne vom instala pachetele pe care le vom folosi, rulând comanda `npm i`

#### **Observație**: 
Ca să vă scutim de treaba foarte enervantă de a tot inchide și apoi deschide server-ul la fiecare modificare, v-am inclus un pachet nou, `nodemon`. Folosind `npx nodemon` sau `nodemon server.js`, server-ul se va reporni de fiecare dată când facem o modificare.

## Cerințe
![](https://media.giphy.com/media/5bzysN9TANWB99yOAB/giphy.gif)

- Crează o bază de date în Laragon cu numele `tema_back-end_2023`
    -**atenție** collation-ul trebuie să fie `ut8mb4_general_ci` 
- Modifică fișierul `utils.js` astfel încât numele bazei de date să corespundă cu cel din cod
- Crează o rută de reset pentru baza de date folosind `utils`
- Inițializează tabelele folosind ruta de reset
- Creează un obiect de tip `Film` și inserează-l în baza de date folosind `utils`
- Creează 3 obiecte de tip `Actor` și inserează-le în baza de date folosind `utils`
------------
**Observație**: toate cerințele de mai jos se realizează folosind Postman, iar pentru marea majoritate ar trebuie să faceți câte o rută separată, acolo unde are sens. 
- Fă o rută pentru a adăuga obiecte de tip `Film` în baza de date.
  - Crează validări pentru această rută
- Creează o rută care apelată să returneze (folosindu-te de response) cât timp mai este până iese un film sau cât timp a trecut
- Fă o rută pentru a adăuga obiecte de tip `Actor` în baza de date
    - crează validări pentru această rută
- Asigurați-vă de unicitatea câmpului ```email``` în tabela `Actori ` atunci când introduceți un actor nou
- Fă o rută care atunci când este apelată, să returneze ce actori au jucat într-un film specificat
- Din bugetul fiecărui film scădeți salariile actorilor și afișați profiturile acestor filme
- Afișați toate filmele în ordine crescătoare a rating-ului.
- Creați o rută care atunci când este apelată premiază filmul cu cel mai mare rating
    - implementați o soluție pentru a reține toate filmele premiate (dacă un film a avut la un moment dat cel mai mare rating,dar nu are în momentul actual cel mai mare rating nu înseamnă că premiul îi este retras) 
- Afișați toate filmele premiate 

### !Nu vă demotivați dacă nu apucați să faceți tot 🕥, trimiteți cât apucați!

 <details>
    <summary><b>Înca ceva</b></summary>
    <br>
    <p> Și nu uitați că puteți căuta mereu pe </p>
    <img src="./nimicAici/Google.jpg">    
    </details>
<br>

### Înainte de a ne trimite tema vă rugăm 🙏 să ștergeți folder-ul ``node_modules``

# ✨Mult Succes✨
![](https://media.giphy.com/media/MDJ9IbxxvDUQM/giphy.gif)

