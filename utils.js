const { Sequelize, DataTypes } = require("sequelize");

const database = new Sequelize("Dacă vezi asta ai uitat un pas ( ͡° ͜ʖ ͡°)", "root", "", {
  dialect: "mysql",
  host: "localhost",
  logging: false,
  define: {
    charset: "utf8",
    collate: "utf8_general_ci",
    timestamps: true,
  },
});

//aici aveti un link spre pagina de sequelize
//pachetul care se ocupa cu manipularea obiectelor si tabelelor
//din baza de date
//https://sequelize.org/docs/v6/getting-started/
//aveti in partea stanga mai multe capitole pe care sa le parcurgeti
//ca sa va faceti o oarecare idee despre ce este si cum functioneaza
const movieDb = database.define("movie", {
  Id: {
    type: DataTypes.BIGINT,
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
  },
  title: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  description: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  dateOfRelease: {
    type: DataTypes.DATE,
    allowNull: false,
  },
  genre: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  budget: {
    type: DataTypes.BIGINT,
    allowNull: false,
  },
  director: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  rating: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  hasAward: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
});

const actorDb = database.define(
  "actor",
  {
    Id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    phone: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    sex: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    role: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    salary: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    dateOfBirth: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    isDebutante: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
  },
  {
    freezeTableName: true,
  }
);

movieDb.hasMany(actorDb);
actorDb.belongsTo(movieDb);

/**
 * Functia creaza tabelul actorilor in baza de date si il goleste daca exista deja.
 * @return
 * Daca operatia a mers, functia va returna 'succes!'
 * Daca operatia esueaza, functia va intoarce 'eroare!'
 */
function resetDatabase() {
  return database
    .sync({ force: true })
    .then(() => {
      return "succes!";
    })
    .catch((reason) => {
      return "eroare";
    });
}

/**
 * Functia intoarce toti userii din baza de date
 * @return
 * Daca operatia a mers, va returna un array cu toti utilizatorii.
 * Daca operatia esueaza, va intoarce 'eroare!'
 */
function getAllActors() {
  return actorDb
    .findAll()
    .then((actors) => {
      return actors.map((actor) => actor.get());
    })
    .catch(() => {
      return "eroare!";
    });
}

/**
 * Functia primeste ca parametru un id (numar intreg) si intoarce actorul cu id-ul mentionat.
 * @param {number} id
 * @return
 * Daca operatia a mers, functia va returna actorul cu id-ul mentionat
 * Daca operatia esueaza, functia va intoarce 'eroare!'
 */
function getActorById(id) {
  return actorDb
    .findByPk(id)
    .then((actor) => {
      return actor.get();
    })
    .catch(() => {
      return "eroare!";
    });
}

/**
 * Functia primeste un actor ca parametru pe care il insereaza in baza de date
 * @return
 * Daca operatia a mers, functia va returna 'succes!'
 * Daca operatia esueaza, functia va intoarce 'eroare!'
 */
function insertActor(actor) {
  return actorDb
    .create(actor)
    .then(() => {
      return "succes!";
    })
    .catch((err) => {
      console.log(err);
      return "eroare!";
    });
}

/**
 * Functia primeste ca parametru un id (numar intreg) si sterge actorul cu id-ul mentionat.
 * @return
 * Daca operatia a mers, functia va returna 'succes!'
 * Daca operatia esueaza, functia va intoarce 'eroare!'
 */
function deleteActorById(id) {
  return actorDb
    .destroy({
      where: {
        id: id,
      },
    })
    .then(() => {
      return "succes!";
    })
    .catch(() => {
      return "eroare!";
    });
}

function getAllMovies() {
  return movieDb
    .findAll()
    .then((actors) => {
      return actors.map((actor) => actor.get());
    })
    .catch(() => {
      return "eroare!";
    });
}

function getMovieById(id) {
  return movieDb
    .findByPk(id)
    .then((movie) => {
      return movie.get();
    })
    .catch(() => {
      return "eroare!";
    });
}

function insertMovie(movie) {
  return movieDb
    .create(movie)
    .then(() => {
      return "succes!";
    })
    .catch((err) => {
      console.log(err);
      return "eroare!";
    });
}

module.exports = {
  resetDatabase,
  getAllActors,
  getActorById,
  insertActor,
  deleteActorById,
  getAllMovies,
  getMovieById,
  insertMovie,
};
